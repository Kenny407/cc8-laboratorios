#Readme.md 

Author: Kenny Alvizuris
Universidad Galileo 2015 - CC8 - Ing. Etson Guerrero.

To execute this program:

1. Move in the command line to the directory lab2
For example: C:\Users\Kenny\CC8\lab3

2. Compile the WebServer.java file
For example: javac WebServer.java

3. Execute the WebServer by entering the next command

java WebServer

4. Open a web browser: Firefox, Chrome, etc.

5. This server is using the 2407 port.

6. Go into 127.0.0.1:2407, or 127.0.0.1:2407/page.html, or an invalid route like 127.0.0.1:2407/route_not_existing

7. It also supports files inside directories, for example 127.0.0.1:2407/ejemplo/page.html

8. Keep doing tests :)


