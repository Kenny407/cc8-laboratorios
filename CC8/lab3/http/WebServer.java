import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.net.Socket;
import java.net.ServerSocket;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class WebServer
{
	public static int serverPort = 2407;
	public static Thread thread;
	public static String configFile = "lab3.conf";

	public static void main(String argv[]) throws Exception
	{	
		System.out.println("Bienvenido al Web Server\nKenny Alvizuris - 12002096\n");
		try{
			ServerSocket welcomeSocket = new ServerSocket(serverPort);
			String readConfigFile = HttpRequest.readConf(configFile);
			Integer numberOfThreads = HttpRequest.checkRegexThreads(readConfigFile);
			ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

			System.out.println("Number of Threads from Config File: " + numberOfThreads);

			while(true){
				try{
					Socket connectionSocket = welcomeSocket.accept();
					HttpRequest http = new HttpRequest(connectionSocket, "Kenny");
					executor.execute(http);
				}catch (IOException e) {
					return;
				}
			}
		}catch (IOException e) {
			System.err.println("Could not listen on port:"+serverPort+".");
		}
	}
}

final class HttpRequest implements Runnable{

	protected Socket clientSocket;
	protected String serverText;
	protected static List<String> lines;

	public HttpRequest(Socket clientSocket, String serverText) {
		this.clientSocket = clientSocket;
		this.serverText = serverText;
	}

	public void run() {
		try {
			DataInputStream  input = new DataInputStream (clientSocket.getInputStream());
			DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
			String read = "";
			String headers = "";

			while ((read = input.readLine()) != null){
				headers = headers + read + "\n";
				if(headers.contains("Connection:")){
					break;
				}
			}

			System.out.println("Headers: \n" + headers);

			String[] response = HttpRequest.checkRegexRequest(headers);
			String type = response[0];
			String route = response[1];
						
			if(route.equals("/")){
				route = "index.html";
			}else{
				route = route.substring(1, route.length());
			}

			System.out.println("Tipo: " + type);
			System.out.println("Ruta: " + route);

			if(!HttpRequest.fileExists(route)){
					type = "404";
			}

			switch(type){
				case "HEAD":
					output.writeBytes("HTTP/1.1 200 OK\n");
					output.writeBytes("Connection close\n");
					output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
					output.writeBytes("Server: " + serverText + "\n");
					output.writeBytes("Last-Modified: Mon, 22 Jun 1998\n");
					output.writeBytes("Content-Length: " + HttpRequest.fileLength(route) + "\n");
					output.writeBytes("Content-Type: text/html; text/css;\n");
					output.writeBytes("\n");
					break;

				case "GET":
					output.writeBytes("HTTP/1.1 200 OK\n");
					output.writeBytes("Connection close\n");
					output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
					output.writeBytes("Server: " + serverText + "\n");
					output.writeBytes("Last-Modified: Mon, 22 Jun 1998\n");
					output.writeBytes("Content-Length: " + HttpRequest.fileLength(route) + "\n");
					output.writeBytes("Content-Type: text/html; image/jpeg;\n");
					output.writeBytes("\n");
					output.writeBytes(HttpRequest.textFromFile(route));
					output.writeBytes("\n");
					break;

				case "POST":
					break;

				case "404":
					route = "404.html";
					output.writeBytes("HTTP/1.1 404 Not Found\n");
					output.writeBytes("Connection close\n");
					output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
					output.writeBytes("Server: " + serverText + "\n");
					output.writeBytes("Last-Modified: Mon, 22 Jun 1998\n");
					output.writeBytes("Content-Length: " + HttpRequest.fileLength(route) + "\n");
					output.writeBytes("Content-Type: text/html; text/css;\n");
					output.writeBytes("\n");
					output.writeBytes(HttpRequest.textFromFile(route));
					output.writeBytes("\n");
					break;
			}

			if (headers.contains("Connection: close")) {
				System.out.println("Cerrando Conexion...\n");
				output.close();
				input.close();
				clientSocket.close();
			}else if(headers.contains("Connection: keep-alive")){
				System.out.println("Server received Keep-alive.\n");
			}

		} catch (IOException e) {
			System.out.println("Error Socket no disponible para IO");
		}
	}

	public static boolean fileExists(String filename){
		File f = null;
		String path = "";
		boolean fileExists = false;
		try{
			f = new File(filename);
		    fileExists = f.exists() && !f.isDirectory();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error no File Exists");
		}
		return fileExists;
	}

	public static int fileLength(String filename){
		int length = 0;
		try {
			lines = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());
			for (String line : lines) {
                length += line.length();
            }
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error file length");
		}
		return length;
	}


	public static String textFromFile(String filename){
		String textFromFile = "";
		try {
			lines = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());
			for (String line : lines) {
                textFromFile += line;
            }
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error text from file");
		}
		return textFromFile;
	}

	public static String readConf(String conf){
		String twoThreads = "MaxThreads=<2>";
		if(HttpRequest.fileExists(conf)){
			return HttpRequest.textFromFile(conf);
		}else{
			try{
				PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
				writer.println(twoThreads);	
			}catch(Exception e){}
			return twoThreads;
		}
	}

	public static Integer checkRegexThreads(String text){
        Pattern pattern = Pattern.compile("MaxThreads=<([1-9][1-9]*)>");
        Matcher matcher = pattern.matcher(text);
        if(matcher.find()){
        	return Integer.parseInt(matcher.group(1));
        }else{
        	System.out.println("MaxThreads not valid, starting the server with 2 threads.");
        	return 2;
        }
	}


	public static String[] checkRegexRequest(String text){
		String[] parts = new String[2];
        Pattern pattern = Pattern.compile("(GET|HEAD|POST) (\\/([a-z]|[A-Z]|[\\--\\/])*) HTTP\\/(1\\.0|1\\.1)");
        Matcher matcher = pattern.matcher(text);
        if(matcher.find()){
        	parts[0] = matcher.group(1);
        	parts[1] = matcher.group(2);
        	return parts;
        }else{
        	return null;
        }
	}

}