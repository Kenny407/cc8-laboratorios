package TCP;
import java.io.*;
import java.net.*;


public class TCPClientApp{
	public int port;
	public String hostname;
	public String sentence;
	public String modifiedSentence;

	public TCPClientApp(int port){
			this.port = port;
			hostname = "localhost";
			BufferedReader inFromUser = new BufferedReader( new InputStreamReader(System.in));   
			Socket clientSocket; 
			while(true){
				try{
					clientSocket = new Socket(hostname, port); 
					DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());   
					BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));   
					sentence = inFromUser.readLine();

					if(sentence.equals("exit"))
						clientSocket.close();

					outToServer.writeBytes(sentence + '\n');   
					modifiedSentence = inFromServer.readLine();   
					System.out.println("From Server: " + modifiedSentence); 

				}catch (UnknownHostException e) {
		            System.err.println("Can't find host: " + hostname);
		            System.exit(1);
		        } catch (IOException e) {
		            System.err.println("Couldn't get I/O for the connection to: " + hostname);
		            System.exit(1);
		        }catch(Exception e){
					System.out.println("Communication not available\nStacktrace:" + e);
				}
				
				  
			} 
	}
}