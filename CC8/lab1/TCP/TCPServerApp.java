package TCP;

import java.io.*;
import java.net.*;

public class TCPServerApp{

	public int port;
	public String clientSentence;
	public String capitalizedSentence;

	public TCPServerApp(int port){
		this.port = port;
		try{
			ServerSocket welcomeSocket = new ServerSocket(port); 
			System.out.println("Server TCP: ON\nWaiting for connection on port: " + port);         
			while(true){             
				Socket connectionSocket = welcomeSocket.accept();             
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));             
				DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());             
				clientSentence = inFromClient.readLine();             
				System.out.println("Received from Client: " + clientSentence);             
				capitalizedSentence = clientSentence.toUpperCase() + '\n';             
				outToClient.writeBytes(capitalizedSentence);          
			}
		}catch(Exception e){
			System.out.println("Connection was refused! Try again.");
		}       
		
	}
}