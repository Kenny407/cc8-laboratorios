#Readme.md 

Author: Kenny Alvizuris
Universidad Galileo 2015 - CC8 - Ing. Etson Guerrero.

To execute this program:

1. Move in the command line to the directory lab1
For example: C:\Users\Kenny\CC8\lab1

2. Compile the UDP and TCP directories
javac UDP/*.java
javac TCP/*.java

3. Compile the 2 main classes: ClientApp and ServerApp

javac ClientApp.java
javac ServerApp.java

4. Open a new command line in the same directory as step 1

5. Execute server by typing this command:
java ServerApp <type_of_connection>


example: java ServerApp UDP or java ServerApp TCP

6. Execute the client
java ClientApp <type_of_connection>

example: java ClientApp UDP or java ClientApp TCP

Special Note:
<type_of_connection> allows: TCP or UDP, any other character should be an error.

If you're using a TCP server, you should instantiate a TCP client.
The same note for UDP connection.

