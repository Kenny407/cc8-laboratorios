package UDP;
import java.io.*;
import java.net.*;


public class UDPServerApp{
	public int socket;

	public UDPServerApp(int socket){
		this.socket = socket;
		try{
			DatagramSocket serverSocket = new DatagramSocket(9876);
			byte[] receiveData = new byte[512];
			byte[] sendData = new byte[512];
			while(true){
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				serverSocket.receive(receivePacket);
				String sentence = new String( receivePacket.getData());
				if(sentence.equals("exit"))
					serverSocket.close();
				System.out.println("RECEIVED: " + sentence);
				InetAddress IPAddress = receivePacket.getAddress();
				int port = receivePacket.getPort();
				String capitalizedSentence = sentence.toUpperCase();
				sendData = capitalizedSentence.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
				serverSocket.send(sendPacket);
			}
		}catch(Exception e){
			System.out.println("Connection was refused! Try again.");
		}
		
	}
}