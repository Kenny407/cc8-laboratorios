package UDP;
import java.io.*;
import java.net.*;


public class UDPClientApp{
	public int port;
	public UDPClientApp(int port){
		this.port = port;
		try{
			while(true){
				BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
				DatagramSocket clientSocket = new DatagramSocket();
				InetAddress IPAddress = InetAddress.getByName("localhost");
				byte[] sendData = new byte[512];
				byte[] receiveData = new byte[512];
				String sentence = inFromUser.readLine();
				sendData = sentence.getBytes();
				if(sentence.equals("exit"))
					clientSocket.close();

				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
				clientSocket.send(sendPacket);
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				clientSocket.receive(receivePacket);
				String modifiedSentence = new String(receivePacket.getData());
				System.out.println("FROM SERVER:" + modifiedSentence);
			}
		}catch(Exception e){
			System.out.println("Connection was refused!");
		}
	}
}