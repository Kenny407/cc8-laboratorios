import java.io.*;
import java.net.*;
import TCP.TCPServerApp;
import UDP.UDPServerApp;


public class ServerApp{
	public static void main(String[] args) throws Exception{
		int tcpPort = 1337;
		int udpPort = 9898;
		if(args.length == 1){
			try{
				String typeOfConnection = args[0];
				typeOfConnection = typeOfConnection.toUpperCase();
				if(typeOfConnection.equals("TCP")){
					System.out.println("TCP Server created on Port: " + tcpPort);
					TCPServerApp tcpServer = new TCPServerApp(tcpPort);
				}else if(typeOfConnection.equals("UDP")){
					System.out.println("UDP Server created on Port" + udpPort);
					UDPServerApp udpServer = new UDPServerApp(udpPort);
				}else{
					System.out.println("Error: Parameter should be TCP or UDP\n");
					ServerApp.help();
				}
			}catch(Exception e){
				System.out.println("Parameter is not a string.");
			}
		}else{
			ServerApp.help();
		}
	}

	public static void help(){
		System.out.println("UDP Server Help");
		System.out.println("Usage of the server:");
		System.out.println("java ServerApp <type_of_connection>");
		System.out.println("Example: java ServerApp UDP");
		System.out.println("Example: java ServerApp TCP");
		System.out.println("And remember to use TCP or UDP for both server/client");
	}
}