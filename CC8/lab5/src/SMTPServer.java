 import java.io.*; 
 import java.net.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
 import java.sql.*;
 import java.util.concurrent.Executors;
 import java.util.concurrent.ExecutorService;


 public class SMTPServer{
	public static int serverPort = 1337;
	public static Thread thread;
	public static int numberOfThreads = 8;
	private static String domainName = "kenny.com";

	public static void main(String argv[]) throws Exception
	{	
		System.out.println("\nSMTP Server on port:" + serverPort + "\nKenny Alvizuris - 12002096\n");
		try{
			ServerSocket welcomeSocket = new ServerSocket(serverPort);
			ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

			while(true){
				try{
					Socket connectionSocket = welcomeSocket.accept();
					SMTPRequest smtp = new SMTPRequest(connectionSocket, domainName);
					executor.execute(smtp);
				}catch (IOException e) {
					System.out.println("Closing server...");
					welcomeSocket.close();
					return;
				}
			}
		}catch (IOException e) {
			System.err.println("Could not listen on port:" + serverPort);
		}
	}	
 }
 
 
 final class SMTPRequest implements Runnable{

		protected Socket clientSocket;
		protected String hostname;
		protected static List<String> lines;
		
		static String response="";
		static int status = 0;
		static UUID idMail = null;
		static String msj="";
		static FileWriter file = null;
		static PrintWriter writer = null;
		static Connection connection;
		static String data = "";
		static String subject="";
		static String para ="";
		static String de ="";
		static Boolean insert = false;
		static LinkedList<String> email = new LinkedList<String>();
		static String domainS="";

		public SMTPRequest(Socket clientSocket, String hostname) {
			this.clientSocket = clientSocket;
			this.hostname = hostname;
		}

		public void run() {
			Scanner sc;
			String msjCliente="";
			Boolean exit = true;
			DataOutputStream server = null;

			try{	
				
				Class.forName("org.sqlite.JDBC");
			
				connection = DriverManager.getConnection("jdbc:sqlite:test.db"); 
			  	System.out.println("");
			  	
			while(exit){  
				sc = new Scanner(new InputStreamReader(clientSocket.getInputStream()));  
				server = new DataOutputStream(clientSocket.getOutputStream());             
				msjCliente = sc.nextLine(); 
				if (msjCliente.equals("QUIT") || msjCliente.equals("quit"))
					exit = false;
				checkAction(msjCliente);
				msj = msjCliente;
				server.writeBytes(response + "\n");
				if(insert){

					
					while(email.size() != 0){
						int email_id = 0;
						para = email.removeFirst();
						System.out.println("lista " + para);
						int i = para.indexOf("@");
						String domain = para.substring(i+1,para.length());
						String username = para.substring(0, i);
						
						/* reviso si el usuario de mi dominio existe */ 
						if(domain.equals(hostname)){
							Statement s = connection.createStatement();								
							String user="SELECT mail FROM users WHERE(name='" + username + "' AND mail='" + domain + "' )";
							System.out.println("query: " + user);
							boolean rs = s.execute(user);
							if(rs){
								System.out.println("De: " + de + "@" + domainS);
								System.out.println("Para: " + para);
								System.out.println("Data: " + data);
								String query = "INSERT INTO correos VALUES (0,'"+ de + "@" + domain + "','" + para + "','" + data + "')";
								System.out.println(query);
								s.execute(query);
								System.out.println("Recibio un email el usuario: " + para);
							}
							else System.out.println("Usuario no encontrado");
							
						}
						else{
							LinkedList<String>  response =  checkDomain(domain);
							System.out.println("Domain Server: " + domain);
							if(!response.isEmpty()){
								String dnsIp = response.get(0);
								int dnsPort = Integer.parseInt(response.get(1));
								
								if(dnsIp.equals("")){
									System.out.println("No se encontro el Dominio");
								}else{
									System.out.println("DNS IP: " + dnsIp + "\nDNS Port: " + dnsPort);
									Socket socket = new Socket(dnsIp, dnsPort);
									DataOutputStream out = new DataOutputStream(socket.getOutputStream());
									BufferedReader recibo = new BufferedReader(new InputStreamReader(socket.getInputStream()));
									out.writeBytes("HELO " + domain + "\r\n");
									recibo.readLine();
									System.out.println("saludo al server");
									out.writeBytes("MAIL FROM: <" + de + "@" + domainS + ">\r\n");
									recibo.readLine();
									System.out.println("Mail From done");
									out.writeBytes("RCPT TO: <" + para + ">\r\n");
									String ok = recibo.readLine();
									System.out.println("Rctp to done");
									if(ok.contains("250")){
										out.writeBytes("DATA\r\n");
										recibo.readLine();
										System.out.println("Data..");
										out.writeBytes(data);
										System.out.println("Data done..");
										out.writeBytes("\n.\n");
										recibo.readLine();
										System.out.println(". done");
										out.writeBytes("QUIT\r\n");
										recibo.readLine();
										System.out.println("quit - bye bye");
										socket.close();
									}else{
										System.out.println("No recibi 250 Ok");
									}
								}
							}
						}
					}
					insert = false;
				}
				server.flush();
			}
			server.close();
			}
			catch (SQLException e){
			   e.printStackTrace();
			}catch (UnknownHostException e) {
				System.err.println("Can't find host: " + hostname);
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Couldn't get I/O for the connection to: " + hostname);
				System.exit(1);
			}catch(ClassNotFoundException cnfe){
				System.err.println("Couldn't find the class JDBC");
				System.exit(1);;
			}

			while(exit){
				try{
					
				}catch(Exception e){
					System.out.println("Communication not available");
					exit = false;
				}	
			}
		}
		
		public static void checkAction(String command){
			String uno = "";
			String dos="";
			String tres="";
			Scanner tokens = new Scanner(command);
			tokens.useDelimiter(" ");
			Boolean first = true;;
			
			if(tokens.hasNext()){
				uno = tokens.next().toUpperCase();
			}
			
			if(tokens.hasNext()){
				dos = tokens.next().toUpperCase();
			}
			if(tokens.hasNext()){
				tres = tokens.next();
			}
			System.out.println("Uno: "+ uno);
			System.out.println("Dos: " + dos);
			System.out.println("Tres: " + tres);
			if((uno.contains("HELO")) && status == 0){
				//if(dos.equals(""))
				//	response = "CANNOT FIND SERVER \r";
				//else{
					response = "250 OK HELO " + dos + " WELCOME TO CONSOLE MAILING \r";
					status=1;
				//}
			}
			
			else if(uno.equals("MAIL") && dos.equals("FROM:") && status == 1){
				if(tres.equals(""))
					response = "NO MAIL FOUND \r";
				else if(tres.startsWith("<") && tres.endsWith(">")){
					int index = tres.indexOf("@");
					de = tres.substring(1,index);
					domainS = tres.substring(index+1, tres.length()-1);
					response = "250 OK\r";
					status = 2;
				}
				else {
					response = "555 5.5.2 Syntax error.\r";
				}
			}
					
			else if((status == 4 || status == 5)){
				if(status == 4){
					try{
						status = 5;
						idMail = UUID.randomUUID();
						file = new FileWriter(idMail + ".txt");
						writer = new PrintWriter(file);
						writer.write("CORREO No. " + idMail + "\r\n" + command );
						response = "";
						data = data + "\n" + command;
					}
					catch(Exception e){
						System.out.println("ERROR CREATING FILE " + e);
					}
				}
				else {
					try{
						System.out.println("Que recibi en el punto: " + uno);
						if(uno.contains(".")){
							response = "250 OK: queued as " + idMail + "\r";
							writer.close();
							//System.out.println("se cerro el file");
							status = 0;
							insert = true;
							writer.write("");
						}else {
							writer.write("\r\n" + command );
							data = data + "\n" + command;
						}
					}
					catch(Exception e){
						System.out.println("ERROR IN WRITING TO FILE " + e);
					}
				}
			}
			
			
			else if(uno.equals("DATA") && status == 3){
				if(first){
					response = "354 End data with <CR><LF>.<CR><LF> \r";
					first = false;
				}
				status = 4;
			}
			else if(uno.equals("RCPT") && dos.equals("TO:") && (status == 2 || status == 3)){
				if(tres.equals(""))
					response = "NO RECIPIENT FOUND \r";
				else if(tres.startsWith("<") && tres.endsWith(">")){
					response = "250 OK\r";
					para = tres.substring(1,tres.length()-1);
					email.add(para);
					
					status = 3;
					System.out.println("para " + para);
				}
				else{
					response = "555 5.5.2 Syntax error.\r";
				}
			}
			
			else if(uno.equals("QUIT")){
				response = "221 BYE \r";
				status = 0;;
			}
			else if(status == 0 && (uno.equals("MAIL") || uno.equals("RCPT") || uno.equals("DATA"))){
				response = "HELO EXPECTED \r";
			}
			else if(status == 1 && (uno.equals("HELO") || uno.equals("RCPT") || uno.equals("DATA"))){
				response = "MAIL FROM: EXPECTED \r";
			}
			else if(status == 2 && (uno.equals("MAIL") || uno.equals("HELO") || uno.equals("DATA"))){
				response = "RCPT TO: EXPECTED \r";
			}
			else if(status == 3 && (uno.equals("MAIL") || uno.equals("RCPT") || uno.equals("HELO"))){
				response = "DATA EXPECTED \r";
			}
			else{
				response = "555 5.5.2 Syntax error.\r";
			}
			tokens.close();
	}
		
		public static String textFromFile(String filename){
			String textFromFile = "";
			try {
				lines = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());
				for (String line : lines) {
	                textFromFile += line;
	            }
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error text from file");
			}
			return textFromFile;
		}
		
		public static LinkedList<String> checkDomain(String domain){
			Connection con = null;
			Statement stmt = null;
			LinkedList<String> resources = new LinkedList<String>(); 
			System.out.println("Checking Domain: Received ->" + domain);
			try {
				
			      Class.forName("org.sqlite.JDBC");
			      con = DriverManager.getConnection("jdbc:sqlite:test.db");
			      con.setAutoCommit(false);
			      System.out.println("Checking Domain: Opened database successfully for translation");

			      stmt = con.createStatement();
			      ResultSet rs = stmt.executeQuery("SELECT * FROM DNS;");
			      while (rs.next()) {
			    	  String dnsDomain = rs.getString("domain");
			    	  System.out.println("Domain: " + dnsDomain);
			    	  if(dnsDomain.equals(domain)){
			    		  resources.add(rs.getString("ip"));
			    		  resources.add(rs.getInt("port")+"");
			    		  System.out.println("Encontre el dominio: " + dnsDomain);
			    	  }
			      }
			      
			      rs.close();
			      stmt.close();
			      con.close();
			    } catch ( Exception e ) {
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			      System.exit(0);
			    }
			    return resources;
		}
}