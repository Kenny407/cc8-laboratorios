package DatabaseMan;

import java.sql.*;

public class newDb {
	public static Connection con = null;
	public static Statement stmt = null;
	
	public static void main(String[] args) {
		createUsers();
		createDNS();
		
		verifyUsers();
		//verifyDNS();
		
	}
		
	
	public static void createUsers(){
		 try {
		      Class.forName("org.sqlite.JDBC");
		      con = DriverManager.getConnection("jdbc:sqlite:test.db");
		      System.out.println("Opened database successfully");

		      stmt = con.createStatement();
		      String sql = "CREATE TABLE users (" +
		                   "name TEXT NOT NULL PRIMARY KEY," +
		                   "mail TEXT NOT NULL" + 
		                   ")"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO users (name,mail) " +
						"VALUES ('kenny', 'kenny.com');"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO users (name,mail) " +
						"VALUES ('jorge', 'kenny.com');"; 
		      stmt.executeUpdate(sql);
		      
		      sql = "INSERT INTO users (name,mail) " +
						"VALUES ('rodrigo', 'kenny.com');"; 
		      stmt.executeUpdate(sql);
			
		      stmt.close();
		      con.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    System.out.println("Created Users table succesfully");
	}
	
	
	public static void createDNS(){
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:test.db");
			con.setAutoCommit(false);
			System.out.println("Opened database successfully");
			
			stmt = con.createStatement();
		    String sql = "CREATE TABLE DNS (" +
		                   "domain TEXT NOT NULL UNIQUE," +
		                   "ip TEXT NOT NULL," +
		                   "port INT NOT NULL" + 
		                   ")"; 
		    stmt.executeUpdate(sql);
		    System.out.println("Created DNS table successfully.");
			
			sql = "INSERT INTO DNS (domain,ip,port) " +
						"VALUES ('kenny.com', '127.0.0.1', 1337);"; 
			stmt.executeUpdate(sql);
			
			sql = "INSERT INTO DNS (DOMAIN,IP,PORT) " +
					"VALUES ('gmail.com', '127.0.0.1', 3000);"; 
			stmt.executeUpdate(sql);

			stmt.close();
			con.commit();
			con.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Inserted successfully");
	}
	
	public static void verifyUsers(){
		try {
		      Class.forName("org.sqlite.JDBC");
		      con = DriverManager.getConnection("jdbc:sqlite:test.db");
		      con.setAutoCommit(false);
		      System.out.println("Opened database successfully");

		      stmt = con.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT * FROM users;" );
		      while ( rs.next() ) {
		    	  String nombre = rs.getString("name");
		    	  String correo = rs.getString("mail");
		    	  
		         System.out.println( "Nombre = " + nombre );
		         System.out.println( "Correo = " + correo );
		         System.out.println();
		      }
		      rs.close();
		      stmt.close();
		      con.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    System.out.println("Created DNS successfully");
	}
	
	public static void verifyDNS(){
		try {
		      Class.forName("org.sqlite.JDBC");
		      con = DriverManager.getConnection("jdbc:sqlite:test.db");
		      con.setAutoCommit(false);
		      System.out.println("Opened database successfully");

		      stmt = con.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT * FROM DNS;" );
		      while ( rs.next() ) {
		    	  String domain = rs.getString("domain");
		    	  String ip = rs.getString("ip");
		    	  int port = rs.getInt("port");
		    	  
		         System.out.println( "Domain = " + domain);
		         System.out.println( "Ip = " + ip);
		         System.out.println( "Port = " + port);
		         System.out.println();
		      }
		      rs.close();
		      stmt.close();
		      con.close();
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    System.out.println("Created DNS successfully");
	}
	
	
}


