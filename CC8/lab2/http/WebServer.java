import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.net.Socket;
import java.net.ServerSocket;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public final class WebServer
{
	public static int serverPort = 2407;
	public static Thread thread;
	public static HttpRequest http;

	public static void main(String argv[]) throws Exception
	{	
		System.out.println("Bienvenido al Web Server\nKenny Alvizuris - 12002096\n");
		try{
			ServerSocket welcomeSocket = new ServerSocket(serverPort);
			while(true){
				try{
					Socket connectionSocket = welcomeSocket.accept();
					http = new HttpRequest(connectionSocket, "Kenny");
					thread = new Thread(http);
					thread.start();
				}catch (IOException e) {
					return;
				}
			}
		}catch (IOException e) {
			System.err.println("Could not listen on port:"+serverPort+".");
		}
	}
}

final class HttpRequest implements Runnable{

	protected Socket clientSocket = null;
	protected String serverText = null;
	protected static List<String> lines = null;

	public HttpRequest(Socket clientSocket, String serverText) {
		this.clientSocket = clientSocket;
		this.serverText = serverText;
	}

	public void run() {
		try {
			long time = System.currentTimeMillis();
			DataInputStream input = new DataInputStream(clientSocket.getInputStream());
			DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());
			String request;

			String petition = input.readLine();
			if(petition.contains("GET") || petition.contains("HEAD")){
				String route = petition.substring(petition.indexOf(' '), petition.lastIndexOf(' ')).trim();

				if(route.equals("/")){
					route = "index.html";
				}else{
					route = route.substring(1, route.length());
				}

				System.out.println("Ruta solicitada: " + route);
				// 404 error not found
				if(!HttpRequest.fileExists(route)){
					route = "404.html";
					output.writeBytes("HTTP/1.1 404 Not Found\n");
					output.writeBytes("Connection close\n");
					output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
					output.writeBytes("Server: " + serverText + "\n");
					output.writeBytes("Last-Modified: Mon, 22 Jun 1998\n");
					output.writeBytes("Content-Length: " + HttpRequest.fileLength(route) + "\n");
					output.writeBytes("Content-Type: text/html\n");
					output.writeBytes("\n");
					output.writeBytes(HttpRequest.textFromFile(route));
					output.writeBytes("\n");

				}else{
					output.writeBytes("HTTP/1.1 200 OK\n");
					output.writeBytes("Connection close\n");
					output.writeBytes("Date: Thu, 06 Aug 1998 12:00:15 GMT\n");
					output.writeBytes("Server: " + serverText + "\n");
					output.writeBytes("Last-Modified: Mon, 22 Jun 1998\n");
					output.writeBytes("Content-Length: " + HttpRequest.fileLength(route) + "\n");
					output.writeBytes("Content-Type: text/html\n");
					output.writeBytes("\n");
					if(petition.contains("GET")){
						output.writeBytes(HttpRequest.textFromFile(route));
						output.writeBytes("\n");
					}
				}
			
				output.flush();
			}			
		} catch (IOException e) {
			System.out.println("Error :");
			e.printStackTrace();
		}
	}

	public static boolean fileExists(String filename){
		File f = null;
		String path = "";
		boolean fileExists = false;
		try{
			f = new File(filename);
		    fileExists = f.exists() && !f.isDirectory();
		}catch(Exception e){
			// if any error occurs
			e.printStackTrace();
		}
		return fileExists;

	}
	public static int fileLength(String filename){
		int length = 0;
		try {
			lines = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());

			for (String line : lines) {
                length += line.length();
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return length;
	}


	public static String textFromFile(String filename){
		String textFromFile = "";
		try {
			lines = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());

			for (String line : lines) {
                textFromFile += line;
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return textFromFile;
	}


}