import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class SMTPServer
{
	public static int serverPort = 23;
	public static Thread thread;
	public static int numberOfThreads = 4;

	public static void main(String argv[]) throws Exception
	{	
		System.out.println("\nSMTP Server on port:" + serverPort + "\nKenny Alvizuris - 12002096\n");
		try{
			ServerSocket welcomeSocket = new ServerSocket(serverPort);
			ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

			while(true){
				try{
					Socket connectionSocket = welcomeSocket.accept();
					SMTPRequest smtp = new SMTPRequest(connectionSocket, "Kenny");
					executor.execute(smtp);
				}catch (IOException e) {
					return;
				}
			}
		}catch (IOException e) {
			System.err.println("Could not listen on port:"+serverPort+".");
		}
	}
}

final class SMTPRequest implements Runnable{

	protected Socket clientSocket;
	protected String hostname;
	public static String domainName = "kenny.google.com";

	public SMTPRequest(Socket clientSocket, String hostname) {
		this.clientSocket = clientSocket;
		this.hostname = hostname;
	}

	public void run() {
		boolean connection = true;
		Pattern patternHelo, patternMailFrom, patternRcptTo, patternData, patternQuit;
		Matcher matcherHelo, matcherMailFrom, matcherRcptTo, matcherData, matcherQuit;
		String unrecognized = "500 Command unrecognized.\n\r";
		String command = "";

		while(connection){
			try{
				DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());   
				DataInputStream  input = new DataInputStream (clientSocket.getInputStream());
				command = input.readLine().toUpperCase();

				System.out.println("Command Received: " + command);
				// HELO local.domain.name or HELO Ip

				patternHelo 	= Pattern.compile("HELO\\s?((([0-9]{1,3}\\.)([0-9]{1,3}\\.)([0-9]{1,3}\\.)([0-9]{1,3})(\\:[0-9]{2,5})?)|(https?\\:\\/\\/(www\\.)?(.+)))?");
				patternMailFrom = Pattern.compile("MAIL FROM:\\s*(.+)@(.+)");
				patternRcptTo 	= Pattern.compile("RCPT TO:\\s*(<?(.+)@(.+)>?)");
				patternData 	= Pattern.compile("DATA");
				patternQuit 	= Pattern.compile("QUIT");

				matcherHelo = patternHelo.matcher(command);
				if(matcherHelo.find()){
					String domain = matcherHelo.group(1);
					outToServer.writeBytes("250, " + domainName + " at your service.\n\r");
					
					command = input.readLine().toUpperCase();
					matcherMailFrom = patternMailFrom.matcher(command);

					if(matcherMailFrom.find()){
						String fromEmail = matcherMailFrom.group(1);
						outToServer.writeBytes("250 "+ domainName +" sender ok.\n\r");

						command = input.readLine().toUpperCase();
						matcherRcptTo = patternRcptTo.matcher(command);

						if(matcherRcptTo.find()){
							String emailTo = matcherRcptTo.group(1);
							outToServer.writeBytes("250 "+ domainName +" recipient ok.\n\r");

							command = input.readLine().toUpperCase();
							matcherData = patternData.matcher(command);

							if(matcherData.find()){
								outToServer.writeBytes("354 " + domainName + " go ahead\n\r");
								String content = "";
								while(!(command = input.readLine().toUpperCase()).equals(".")){
									content = content + command;
								}
								outToServer.writeBytes("250 " + domainName + "message accepted for delivery\n\r");

							}else{
								outToServer.writeBytes(unrecognized);
							}
						}else{
							outToServer.writeBytes(unrecognized);
						}
					}else{
						outToServer.writeBytes(unrecognized);
					}
				}else{
					outToServer.writeBytes(unrecognized);
				}

				// pattern = Pattern.compile("");
				// matcher = pattern.matcher(command);
				
			}catch (UnknownHostException e) {
				System.err.println("Can't find host: " + hostname);
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Couldn't get I/O for the connection to: " + hostname);
				System.exit(1);
			}catch(Exception e){
				System.out.println("Communication not available");
				connection = false;
			}	
		}
	}
}