import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;

public class Test {

private static List<Node> nodes;
private static List<Edge> edges;
private static int numberOfNodes = 6;
private static String[] assets = new String[3];
private static String file = "nodes.txt";

	public static void main(String[] args) throws Exception{
	
		nodes = new ArrayList<Node>();
		edges = new ArrayList<Edge>();
		for (int i = 0; i < 6; i++) {
			Node location = new Node("Node_" + i, "Node_" + i);
			nodes.add(location);
		}

		readFile(file);

		for (int i = 0; i < edges.size() ; i++ ) {
			System.out.println("Edge: " + i + " "+ edges.get(i));
		}

		// Lets check from location Loc_1 to Loc_10
		Graph graph = new Graph(nodes, edges);
		LinkState linkState = new LinkState(graph);
		linkState.execute(nodes.get(0));
		LinkedList<Node> path = linkState.getPath(nodes.get(5));

		System.out.println("Shortest Path");
		for (Node vertex : path) {
			System.out.println(vertex);
		}
	}

	private static void addEdge(String laneId, int sourceLocNo, int destLocNo, int duration) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration);
		edges.add(lane);
	}

	private static void readFile(String filename){
		try{
			int edgeNumber = 0;
			FileReader inputFile = new FileReader(filename);
			BufferedReader bufferReader = new BufferedReader(inputFile);

			//Variable to hold the one line data
			String line;
			Scanner scanner;
			while ((line = bufferReader.readLine()) != null){
				scanner = new Scanner(line);
				scanner.useDelimiter(",");
				if(scanner.hasNext()){
					assets[0] = scanner.next().trim();
					if(scanner.hasNext()){
						assets[1] = scanner.next().trim();
					}
					if(scanner.hasNext()){
						assets[2] = scanner.next().trim();
					}
				}
				addEdge("Edge # " + edgeNumber, Integer.parseInt(assets[0]), Integer.parseInt(assets[1]), Integer.parseInt(assets[2]));
				edgeNumber++;
			}
			//Close the buffer reader
			bufferReader.close();
		}catch(Exception e){
			System.out.println("Error while reading file line by line:" + e.getMessage());                      
		}
	}
} 